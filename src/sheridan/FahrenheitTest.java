package sheridan;

//@Author Timothy Marasigan 991321050

import static org.junit.Assert.*;
import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromFahrenheitRegular() {
		int result = Fahrenheit.fromCelsius(35);
		assertTrue("Invalid num value", result == 95);
	}	
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int result = Fahrenheit.fromCelsius(1);
		assertTrue("Invalid num value", result == 34);
	}
	
	@Test
	public void testFromFahrenheitException() {		
		int result = Fahrenheit.fromCelsius(0);
		assertFalse("Invalid num value", result == 31);
	}

	@Test
	public void testFromFahrenheitBoundaryOut() {		
		int result = Fahrenheit.fromCelsius(-1);
		assertFalse("Invalid num value", result == 29);
	}

}
