package sheridan;

public class Fahrenheit {
	public static int fromCelsius(int num) {
		double value = ( (double) num * 9/5) + 32;
		return (int) Math.round(value);
	}
}
